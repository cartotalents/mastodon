import Rails from '@rails/ujs';

export function start() {
  require('font-awesome/css/font-awesome.css');
  // require('../styles/carto/mapbox.css');
  require('mapbox-gl/dist/mapbox-gl.css')
  require.context('../images/', true);

  try {
    Rails.start();
  } catch (e) {
    // If called twice
  }
};
