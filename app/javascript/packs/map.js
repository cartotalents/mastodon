import loadPolyfills from '../mastodon/load_polyfills';
import { start } from '../mastodon/common';
require('../styles/carto/map.scss').default;
// import 'mapbox-gl/dist/mapbox-gl.css'; // require('mapbox-gl/dist/mapbox-gl.css')


start();

function loaded() {
  const MapContainer = require('../carto/components/map/map.js').default;
  const React = require('react');
  const ReactDOM = require('react-dom');
  const mountNode = document.getElementById('carto-map');

  if (mountNode !== null) {
    const props = JSON.parse(mountNode.getAttribute('data-props'));
    ReactDOM.render(<MapContainer {...props} />, mountNode);
  }
}

function main() {
  const ready = require('../mastodon/ready').default;
  ready(loaded);
}

loadPolyfills().then(main).catch(error => {
  console.error(error);
});
