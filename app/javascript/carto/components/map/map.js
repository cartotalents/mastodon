import ReactDOMServer from 'react-dom/server';
import React, { useEffect, useRef } from 'react';
import mapboxgl from "mapbox-gl";
// require('../../../styles/carto/map.scss');

//Using next.js scoped css for injection on newly created marker, which isn't scoped by default
//https://nextjs.org/docs/basic-features/built-in-css-support#adding-component-level-css
// import moduleStyles from './Map.module.css'

//Récupération du token mapbox
mapboxgl.accessToken = "pk.eyJ1IjoiY2FydG90YWxlbnRzIiwiYSI6ImNrOXd1ejZudjA1OTUzb2xpaGwycWU4bWEifQ.tgBMLcSiOrAOtpcnEXAn7A"; //ENV['MAPBOX_TOKEN'];

// interface MarkerProps {
//   id: any,
//   geoLon: number,
//   geoLat: number,
//   name?: string,
//   email?: string,
//   /**
//    * Possible instance id, where the data comes from
//    */
//   remote_url?: string
// }
// interface MapProps {
//   /**
//    * Array of markers to display at map render.
//    */
//   markers?: [MarkerProps]

//   /**
//    * Optional style override
//    */
//   style?: {}
// }

const defaultMapCenter = {
  lng: -1.569431,
  lat: 47.204438,
  zoom: 4
}

const bounds = {
  lng: { min: -1.620237, max: -1.491491},
  lat: { min: 47.255781, max: 47.183028},
}
const generateCoord = () => {
  return {
    lng: bounds.lng.min + (bounds.lng.min - bounds.lng.max) * Math.random(),
    lat: bounds.lat.min + (bounds.lat.min - bounds.lat.max) * Math.random(),
  }
}
const getInitials = (username) => {
  return username.replace(/[^A-Z]/g, "")
}
const generateUserMarkerMarkup = (marker) => {
  return ReactDOMServer.renderToStaticMarkup(
    <img className={marker.remote_url ? 'remote-border' : 'local-border'} src={marker.avatar_remote_url || "https://www.gravatar.com/avatar/?d=robohash&s=30"} />
  )
}
const generatePopupMarkup = (user) => {
  return ReactDOMServer.renderToStaticMarkup(<div>
    <h2>{user.username}</h2>
    <p>{user.email}</p>
    {typeof user.remote_url == "string" ? (
      <div>
        <p>Remote user from {user.remote_url}</p>
        {/* <a className={`button ${user.remote_url ? 'remote-background' : 'local-background'}`} href={`//${user.remote_url}/user/${user.id.replace(`${user.remote_url}.`, '')}`}>See on remote site</a> */}
      </div>
    ) : (
        <div>
          <p>Local user</p>
          {/* <a href="/user/[id]" className={`button ${user.remote_url ? 'remote-background' : 'local-background'}`}>See more</a> */}
        </div>
      )}
  </div>
  )
}
const generatePopup = (marker) => {
  return new mapboxgl.Popup({ offset: 25 }).setHTML(generatePopupMarkup(marker))
}
const Map = (props) => {
  const { markers, style = {} } = props;
  const mapContainerHTML = useRef(null);
  const map = useRef(null);

  useEffect(() => {
    map.current = new mapboxgl.Map({
      container: mapContainerHTML.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [defaultMapCenter.lng, defaultMapCenter.lat],
      zoom: defaultMapCenter.zoom
    });
  }, [])

  useEffect(() => {
    if (map && markers.length) {
      markers.forEach(marker => {

        // create a HTML element for each feature
        var el = document.createElement('div');
        // el.className = moduleStyles.marker;
        el.innerHTML = generateUserMarkerMarkup(marker);
        const coords = marker.fields.length && marker.fields[0].name === "Géolocalisation" ? marker.fields[0].value.split(",").reverse() : Object.values(generateCoord());
        debugger

        new mapboxgl.Marker(el)
          .setLngLat(coords)
          // .setLngLat([marker.geoLon, marker.geoLat])
          .setPopup(generatePopup(marker))
          .addTo(map.current);
      })
    }
  }, [markers])
  return (
    <div className="mapWrapper" style={style}>
      <div ref={el => {mapContainerHTML.current = el}} className="mapContainer" />
    </div>
  )
}

export default Map
