require 'rails_helper'

RSpec.describe "Maps", type: :request do

  describe "GET /index" do
    it "returns http success" do
      get "/map/index"
      expect(response).to have_http_status(:success)
    end
  end

end
