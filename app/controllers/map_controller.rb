# frozen_string_literal: true
#https://guides.rubyonrails.org/action_controller_overview.html
class MapController < ApplicationController
  #Inhérite un layout (header, footer etc…) dans app/views/layouts
  layout 'public'
  #Filter actionné avant l'action render
  before_action :set_accounts

  #Répond à la route demandée
  def index
    #Rendu
    render :index
  end

  #Méthode appellée avant l'envoi des données au client, via before_action
  def set_accounts
    @accounts = Account.local
  end
end
