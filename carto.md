# Installation

Installation d'une VM Ubuntu 18.04 server LTS via VMWare fusion.

Suivi des étapes Prerequisites sur la VM : https://docs.joinmastodon.org/admin/install/#pre-requisites

Suivi des étapes d'environnement de dev : https://docs.joinmastodon.org/dev/setup/

Login : maen / t

## Liste des commandes 

```
1  
    2  pwd
    3  history
    4  ls 
    5  apt install open-vm-tools
    6  sudo apt install open-vm-tools
    7  ls 
    8  ls ..
    9  
   10  sudo apt-get autoremove open-vm-tools
   11  sudo apt install open-vm-tools-desktop
   12  ls -al
   13  curl -sL https://deb.nodesource.com/setup_12.x | bash -
   14  su -
   15  su 
   16  sudo curl -sL https://deb.nodesource.com/setup_12.x | bash -
   17  ls -al
   18  ls -al /var/lib/apt/lists/lock
   19  su 
   20  apt-get update
   21  sudo apt-get update
   22  sudo curl -sL https://deb.nodesource.com/setup_12.x | bash -
   23  sudo (curl -sL https://deb.nodesource.com/setup_12.x | bash -)
   24  sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
   25  sudo -s
   26  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
   27  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
   28  apt update
   29  apt install -y   imagemagick ffmpeg libpq-dev libxml2-dev libxslt1-dev file git-core   g++ libprotobuf-dev protobuf-compiler pkg-config nodejs gcc autoconf   bison build-essential libssl-dev libyaml-dev libreadline6-dev   zlib1g-dev libncurses5-dev libffi-dev libgdbm5 libgdbm-dev   nginx redis-server redis-tools postgresql postgresql-contrib   certbot python-certbot-nginx yarn libidn11-dev libicu-dev libjemalloc-dev
   30  adduser --disabled-login mastodon
   31  su - mastodon
   32  history
   33  sudo -u postgres psql
   34  ls -al
   35  cd /var/www/
   36  su - mastodon
   37  su - mastodonsudo git clone https://github.com/tootsuite/mastodon.git live && cd live
   38  sudo git clone https://github.com/tootsuite/mastodon.git live && cd live
   39  chown -R mastodon:maen .
   40  ls -al
   41  cd ..
   42  su - mastodon
   43  apt install ruby-railties
   44  sudo apt install ruby-railties
   45  apt remove ruby-railties
   46  sudo apt remove ruby-railties
   47  sudo sudo apt remove ruby-railties
   48  sudo  gem install rails bundler foreman
   49  cp /home/mastodon/live/dist/nginx.conf /etc/nginx/sites-available/mastodon
   50  cp /var/www/live/dist/nginx.conf /etc/nginx/sites-available/mastodon
   51  sudo cp /var/www/live/dist/nginx.conf /etc/nginx/sites-available/mastodon
   52  sudo ln -s /etc/nginx/sites-available/mastodon /etc/nginx/sites-enabled/mastodon
   53  ls -al /etc/nginx/sites-available/mastodon 
   54  sudo nano /etc/nginx/sites-available/mastodon 
   55  sudo systemctl restart nginx
   56  systemctl status nginx.service
   57  sudo systemctl restart nginx
   58  sudo nano /etc/nginx/sites-available/mastodon 
   59  sudo systemctl restart nginx
   60  systemctl status nginx.service
   61  sudo nano /etc/nginx/sites-available/mastodon 
   62  sudo systemctl restart nginx
   63  sudo nano /etc/nginx/sites-available/mastodon 
   64  sudo systemctl restart nginx
   65  sudo nano /etc/nginx/sites-available/mastodon 
   66  sudo systemctl restart nginx
   67  sudo nano /etc/nginx/sites-available/mastodon 
   68  sudo systemctl restart nginx
   69  sudo nano /etc/nginx/sites-available/mastodon 
   70  sudo systemctl restart nginx
   71  sudo nano /etc/nginx/sites-available/mastodon 
   72  systemctl status nginx.service
   73  curl -I 1:3000
   74  sudo nano /etc/nginx/sites-available/mastodon 
   75  sudo systemctl restart nginx
   76  systemctl status nginx.service
   77  sudo nano /etc/nginx/sites-available/mastodon 
   78  sudo systemctl restart nginx
   79  cd /var/www/live/
   80  ls -al
   81  sudo nano /etc/nginx/sites-available/mastodon 
   82  sudo systemctl restart nginx
   83  pwd
   84  history
   85  pwd
   86  ls -al /dev/
   87  mount -g
   88  mount -h
   89  fstab -h
   90  cat /proc
   91  cat /proc/mounts 
   92  ls -al /mnt/
   93  nano /etc/fstab 
   94  vmware-hgfsclient
   95  $ sudo vmhgfs-fuse .host:/my-shared-folder /mnt/hgfs/ -o allow_other -o uid=1000
   96  sudo vmhgfs-fuse .host:/maen /mnt/hgfs/ -o allow_other -o uid=1000
   97  sudo vmhgfs-fuse .host:/ /mnt/ -o allow_other -o uid=1000
   98  sudo vmhgfs-fuse .host:/maen /mnt/hgfs/ -o allow_other -o uid=1000
   99  ls -al /mnt/
  100  ls -al /mnt/maen/
  101  cd /mnt/maen/Dev/Carto_v2/
  102  ln -s /var/www/live vm_live
  103  ln -s /var/www/live 
  104  cp /var/www/live vm_live
  105  cp -f /var/www/live vm_live
  106  cp -r /var/www/live vm_live
  107  sudo mv /var/www/live /var/www/live_local
  108  sudo ln -s /mnt/maen/Dev/Carto_v2/vm_live/ /var/www/live
  109  ls -al /var/www/
  110  ls -al /var/www/live
  111  ls -al /var/www/live/
  112  
  113  vmware
  114  openssh
  115  open-ss
  116  sudo apt install openssh-server
  117  sudo systemctl enable ssh && sudo systemctl start ssh
  118  ipconfig
  119  ifconfig -a
  120  su 
  121  sudoers
  122  cd /etc/apache2/
  123  ls -al
  124  cd ..
  125  ls -al nginx/
  126  cd nginx/
  127  nano sites-enabled/mastodon 
  128  sudo systemctl restart nginx
  129  nano sites-enabled/mastodon 
  130  systemctl status nginx.service
  131  nano sites-enabled/mastodon 
  132  sudo systemctl restart nginx
  133  nano sites-enabled/mastodon 
  134  ls -al
  135  ls -al /etc/ssl/certs/
  136  nano sites-enabled/mastodon 
  137  sudo systemctl restart nginx
  138  ls -al /etc/ssl/certs/ssl-*
  139  ls -al /etc/ssl/certs/*snake*
  140  ls -al /etc/ssl/private/*snake*
  141  nano sites-enabled/mastodon 
  142  sudo systemctl restart nginx
  143  systemctl status nginx.service
  144  nano sites-enabled/mastodon 
  145  sudo systemctl restart nginx
  146  nano sites-enabled/mastodon 
  147  sudo systemctl restart nginx
  148  nano sites-enabled/mastodon 
  149  sudo systemctl restart nginx
  150  nano sites-enabled/mastodon 
  151  sudo systemctl restart nginx
  152  open /etc/ssl/certs/ssl-cert-snakeoil.pem 
  153  cp /etc/ssl/certs/ssl-cert-snakeoil.pem /var/www/live/
  154  cd -
  155  cd /var/www/live
  156  exit
  157  su - mastodon
  158  history
  159  su - mastodon
  160  su mastodon
  161  cd -
  162  cd /etc/nginx/
  163  ls -al
  164  cd sites-enabled/
  165  ls -al
  166  nano mastodon 
  167  sudo systemctl restart nginx
  168  su - mastodon
  169  history
  170  su -mastodon
  171  su - mastodon
  172  su mastodon
  173  su - mastodon
  174  sudo -s 
  175  p
  176  pwd
  177  p∂
  178  ls -al
  179  history
  180  sudo vmhgfs-fuse .host:/maen /mnt/hgfs/ -o allow_other -o uid=1000
  181  sudo vmhgfs-fuse .host:/ /mnt/ -o allow_other -o uid=1000
  182  nano /etc/fstab 
  183  sudo nano /etc/fstab 
  184  ls -al
  185  sudo -s 
  186  sudo -s
  187  cd /var/www/live
  188  yarn run build:development
  189  sudo -s
  190  su - mastodon
  191  su - mastodon
  192  su - mastodon
  193  sudo -s && su - mastodon
  194  history

```

Lancement via utilisateur mastodon 

```
    1  git clone https://github.com/rbenv/rbenv.git ~/.rbenv
    2  cd ~/.rbenv && src/configure && make -C src
    3  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
    4  echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    5  exec bash
    6  git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
    7  RUBY_CONFIGURE_OPTS=--with-jemalloc rbenv install 2.6.6
    8  gem install bundler --no-document
    9  which
   10  which gem
   11  pwd
   12  shims/gem install bundler --no-document
   13  ./shims/gem install bundler --no-document
   14  cd shims/
   15  ls -la
   16  gem
   17  ./ge
   18  ./gem
   19  rbenv rehash
   20  gem install bundler --no-document
   21  rbenv global 2.6.6
   22  gem install bundler --no-document
   23  exit
   24  pwd
   25  cd /var/www/
   26  git clone https://github.com/tootsuite/mastodon.git live && cd live
   27  sudo
   28  ls -al
   29  sudo git clone https://github.com/tootsuite/mastodon.git live && cd live
   30  exit
   31  ls )al
   32  cd /var/www/
   33  ls -al
   34  cd live/
   35  git checkout $(git tag -l | grep -v 'rc[0-9]*$' | sort -V | tail -n 1)
   36  bundle install -j$(getconf _NPROCESSORS_ONLN)
   37  yarn install --pure-lockfile
   38  RAILS_ENV=production bundle exec rake mastodon:setup
   39  bundle install
   40  rails db:setup 
   41  bundle install --with development,test
   42  bundle exec rails db:setup
   43  foreman start
   44  pwd
   45  cd ..
   46  mv live live_local
   47  sudo mv live live_local
   48  cd live
   49  foreman start
   50  ls -al
   51  history
   52  foreman start
   53  cd /var/www/live_local/
   54  cd ..
   55  ls -al
   56  exit
   57  cd /var/www/live
   58  ls -al
   59  rails generate controller Map index
   60  tootctl accounts modify --confirm maen
   61  ./tootctl accounts modify --confirm maen
   62  ./bin/tootctl accounts modify --confirm maen
   63  git log -n 2
   64  git log -n 10
   65  git status
   66  git branch carto_map
   67  git status
   68  git checkout carto_map
   69  git status
   70  git add .
   71  git status
   72  git add app/views/map/index.html.haml
   73  git commit -m "Added a new view, Map, listing all local users"
   74  git config --global user.email "mjuganaikloo@gmail.com"
   75  git config --global user.name "Maen Juganaikloo"
   76  git commit -m "Added a new view, Map, listing all local users"
   77  npm i mapbox-gl --save
   78  ls -al node_modules/
   79  git status
   80  git diff
   81  git diff package-lock.json
   82  npm i express
   83  cm 
   84  cp ../live_local/node_modules/express node_modules/
   85  cp -r ../live_local/node_modules/express node_modules/
   86  cp -r ../live_local/node_modules/express node_modules/express
   87  ls ../live_local/node_modules/express
   88  ls -al ../live_local/node_modules/express
   89  ls -al ../live_local/node_modules/exp*
   90  cd ..
   91  ls -al live_local/node_modules/exp*
   92  cp -r live_local/node_modules/express live/node_modules/express
   93  cd live
   94  npm i express
   95  yarn install
   96  cd ..
   97  cp -r live_local/node_modules live/node_modules
   98  cd live_local/
   99  yarn add mapbox-gl
  100  cd ..
  101  cp -r live_local/node_modules live/node_modules
  102  ls -al live_local/node_modules/
  103  cd live
  104  yarn install --pure-lockfile
  105  cd ../live
  106  cd ..
  107  cd live_local/
  108  yarn install --pure-lockfile && yarn add mapbox-gl
  109  cd ..
  110  cp -r live_local/node_modules live/node_modules
  111  cd live_local/
  112  yarn install --pure-lockfile
  113  mv node_modules node_modules_backup
  114  yarn install --pure-lockfile && yarn add mapbox-gl
  115  cd ..
  116  cp -r live_local/node_modules live/node_modules
  117  cd live
  118  cd ..
  119  cd live_local/
  120  foreman start
  121  ls -al node_modules/uuid
  122  ls -al node_modules/
  123  cd ../live
  124  ls -al node_modules/uuid
  125  ls -al node_modules/
  126  mv node_modules/ node_modules.backup
  127  cd ..
  128  cp -r live_local/node_modules live/node_modules
  129  cd live
  130  webpack
  131  yarn run build:development
  132  yarn run watch:development
  133  yarn run build:development
  134  webpack
  135  yarn run build:development
  136  cd .
  137  ls -al
  138  ls -al dist/
  139  ls -al public/
  140  ls -la
  141  ./bin/tootctl accounts modify --confirm maen
  142  foreman start
  143  cd /var/www/live
  144  foreman start
  145  yarn -h
  146  yarn install mapbox-gl
  147  yarn add mapbox-gl
  148  foreman start
  149  cd ..
  150  cd live
  151  foreman start
  152  yard add uuid
  153  yarn add uuid
  154  foreman start
  155  cd /var/www/live
  156  ./bin/tootctl accounts modify --confirm maen
  157  postgre
  158  postgresql10.postgres 
  159  rails console
  160  cd /var/www/live
  161  foreman start
  162  history

```